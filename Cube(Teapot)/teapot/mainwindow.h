#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSlider>
#include <QWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
//protected:
//    void keyPressEvent(QKeyEvent *event);
private:
    Ui::MainWindow *ui;
    QSlider *createSlider();

    QSlider *xrotobject;
    QSlider *yrotobject;
    QSlider *zrotobject;
};


#endif // MAINWINDOW_H
