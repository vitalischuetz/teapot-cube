#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWidgets>
#include "glwidget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(xrotobject, SIGNAL(valueChanged(int)), GLWidget, SLOT(setXRotation(int)));
    connect(GLWidget, SIGNAL(xRotationChanged(int)), xrotobject, SLOT(setValue(int)));
    connect(yrotobject, SIGNAL(valueChanged(int)), GLWidget, SLOT(setYRotation(int)));
    connect(GLWidget, SIGNAL(yRotationChanged(int)), yrotobject, SLOT(setValue(int)));
    connect(zrotobject, SIGNAL(valueChanged(int)), GLWidget, SLOT(setZRotation(int)));
    connect(GLWidget, SIGNAL(zRotationChanged(int)), zrotobject, SLOT(setValue(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        close();
    else
        QWidget::keyPressEvent(e);
}
