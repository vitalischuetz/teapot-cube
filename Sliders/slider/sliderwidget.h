#ifndef SLIDERWIDGET_H
#define SLIDERWIDGET_H

#include <QMainWindow>

namespace Ui {
class SliderWidget;
}

class SliderWidget : public QMainWindow
{
    Q_OBJECT

public:
    explicit SliderWidget(QWidget *parent = 0);
    ~SliderWidget();

private:
    Ui::SliderWidget *ui;
};

#endif // SLIDERWIDGET_H
