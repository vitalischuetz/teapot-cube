#include "sliderwidget.h"
#include "ui_sliderwidget.h"

SliderWidget::SliderWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SliderWidget)
{
    ui->setupUi(this);
    connect(ui->horizontalSlider,SIGNAL(valueChanged(int)),ui->lcdNumber,SLOT(display(int)));
}

SliderWidget::~SliderWidget()
{
    delete ui;
}
