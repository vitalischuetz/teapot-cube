#-------------------------------------------------
#
# Project created by QtCreator 2014-06-12T19:50:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = slider
TEMPLATE = app


SOURCES += main.cpp\
        sliderwidget.cpp

HEADERS  += sliderwidget.h

FORMS    += sliderwidget.ui
